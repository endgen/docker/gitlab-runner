FROM gitlab/gitlab-runner:alpine

RUN apk update
RUN apk add shadow

RUN addgroup -g 120 docker
RUN addgroup -g 1000 gitlab-runner
RUN addgroup gitlab-runner docker
RUN usermod -u 1000 -g 1000 gitlab-runner
